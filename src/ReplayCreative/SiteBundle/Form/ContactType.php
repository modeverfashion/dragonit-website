<?php

namespace ReplayCreative\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('name')
            ->add('message')
            ->add('created_at')
            ->add('leave_blank', 'hidden', array('mapped' => false, 'data' => '1234'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ReplayCreative\SiteBundle\Entity\Contact'
        ));
    }

    public function getName()
    {
        return 'replaycreative_sitebundle_contacttype';
    }
}
