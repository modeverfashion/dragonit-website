jQuery(document).ready(function ($) {
	// Superfish
		$('ul.sf-menu').superfish({
			delay: 100,
			animation: {opacity:'show',height:'show'},
			speed: 200,
			autoArrows: true
		});

	// TinyNav
		$(function () {
			$('nav ul.sf-menu').tinyNav({
				active: 'selected',
				label: ''
			});
		});

	// Tabs
		var tabs = jQuery('ul.tabs');
		tabs.each(function(i) {
			// get tabs
			var tab = jQuery(this).find('> li > a');
			tab.click(function(e) {
				// get tab's location
				var contentLocation = jQuery(this).attr('href');
				// Let go if not a hashed one
				if(contentLocation.charAt(0)=="#") {
					e.preventDefault();
					// add class active
					tab.removeClass('active');
					jQuery(this).addClass('active');
					// show tab content & add active class
					jQuery(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
				}
			});
		});

	// Accordion
		jQuery("ul.tt-accordion li").each(function(){
			jQuery(this).children(".accordion-content").css('height', function() { 
				return jQuery(this).height(); 
			});
			
			if (jQuery(this).index() > 0) {
				jQuery(this).children(".accordion-content").css('display','none');
			} else {
				jQuery(this).addClass('active').find(".accordion-head-sign").html("&minus;");
				jQuery(this).siblings("li").find(".accordion-head-sign").html("&#43;");
			}
			
			jQuery(this).children(".accordion-head").bind("click", function() {
				jQuery(this).parent().addClass(function() {
					if (jQuery(this).hasClass("active")) return "";
					return "active";
				});
				jQuery(this).siblings(".accordion-content").slideDown();
				jQuery(this).parent().find(".accordion-head-sign").html("&minus;");
				jQuery(this).parent().siblings("li").children(".accordion-content").slideUp();
				jQuery(this).parent().siblings("li").removeClass("active");
				jQuery(this).parent().siblings("li").find(".accordion-head-sign").html("&#43;");
			});
		});

	// Toggle
		jQuery("ul.tt-toggle li").each(function(){
			jQuery(this).children(".toggle-content").css('height', function() { 
				return jQuery(this).height(); 
			});
			
			jQuery(this).children(".toggle-content").css('display','none');
			jQuery(this).find(".toggle-head-sign").html("&#43;");
			
			jQuery(this).children(".toggle-head").bind("click", function() {
			
				if (jQuery(this).parent().hasClass("active")) jQuery(this).parent().removeClass("active");
				else jQuery(this).parent().addClass("active");
				
				jQuery(this).find(".toggle-head-sign").html(function() {
					if (jQuery(this).parent().parent().hasClass("active")) return "&minus;";
					else return "&#43;";
				});
				jQuery(this).siblings(".toggle-content").slideToggle();
			});
		});
		jQuery("ul.tt-toggle").find(".toggle-content.active").siblings(".toggle-head").trigger('click');

	// Carousel
		$('#project-carousel').jcarousel({
			buttonNextHTML: '<div class="dikme"></div>',
			buttonPrevHTML: '<div class="dikme"></div>',
			scroll: 1,
		});

	// Basic Slider
		$('#slider-basic').bjqs({
			showcontrols : true, 
			centercontrols : true, // center controls verically
			nexttext : '<img src="images/right.png" style="height: 10px !important; width:32px !important;">', // Text for 'next' button (can use HTML)
			prevtext : '<img src="images/left.png" style="height: 10px !important; width:32px !important;">', // Text for 'previous' button (can use HTML)
			showmarkers : false, // Show individual slide markers
			keyboardnav : true, 
			'height' : 382, 
			responsive  : true,
		});
		$('#client').bjqs({
			showcontrols : true, 
			centercontrols : true, // center controls verically
			nexttext : '<div class="slider-button slider-next"></div>', // Text for 'next' button (can use HTML)
			prevtext : '<div class="slider-button slider-prev"></div>', // Text for 'previous' button (can use HTML)
			showmarkers : false, // Show individual slide markers
			keyboardnav : true, 
			'width' : 323, 
			'height' : 200, 
			responsive  : true,
		});
		$('#post-slider').bjqs({
			showcontrols : true, 
			centercontrols : true, // center controls verically
			nexttext : '<div class="slider-button slider-next"></div>', // Text for 'next' button (can use HTML)
			prevtext : '<div class="slider-button slider-prev"></div>', // Text for 'previous' button (can use HTML)
			showmarkers : false, // Show individual slide markers
			keyboardnav : true, 
			'width' : 667, 
			'height' : 305, 
			responsive  : true,
		});

	// ExtraSearch
		$('.extraicon').click(function(){
			if( $('#extraInfo').is(':visible') ) {
				$('#extraInfo').slideUp('500', '', '');
				$('a', this).removeClass('close-icon').addClass('search-icon');
			} else {
				$('#extraInfo').slideDown('500', '', '');
				$('a', this).removeClass('search-icon').addClass('close-icon');
			}
			return false;
		});

	// Flickr, You can find your flickr id from idgettr.com
		$('#flickr-photos').jflickrfeed({
			limit: 12,
			qstrings: {
				id: '52617155@N08'
			},
			itemTemplate: 
			'<li>' +
				'<a href="{{image_b}}" class="lightbox[flickr]"><img src="{{image_s}}" alt="{{title}}" /></a>' +
			'</li>'
		});

	// ToTop
		jQuery(window).scroll(function() {
			if(jQuery(this).scrollTop() != 0) {
				jQuery('#toTop').fadeIn();
			} else {
				jQuery('#toTop').fadeOut();
			}
		});
		jQuery('#toTop').click(function() {
			jQuery('body,html').animate({scrollTop:0},800);
		});

	// HoverDir
		jQuery(function() {
			jQuery('ul.da-thumbs > li').hoverdir();
		});

	// Notification
		$(".notification-close-info").click(function(){
			$(".notification-box-info").fadeOut("slow");return false;
		});
		$(".notification-close-success").click(function(){
			$(".notification-box-success").fadeOut("slow");return false;
		});
		$(".notification-close-warning").click(function(){
			$(".notification-box-warning").fadeOut("slow");return false;
		});
		$(".notification-close-error").click(function(){
			$(".notification-box-error").fadeOut("slow");return false;
		});

	// Tipsy
		$(function() {
			$('.top').tipsy({fade: true, gravity: 's'});
			$('.bottom').tipsy({fade: true, gravity: 'n'});
			$('.right').tipsy({fade: true, gravity: 'w'});
			$('.left').tipsy({fade: true, gravity: 'e'});
		});

	var $portfolioClone = $(".portfolio").clone();
	// Attempt to call Quicksand on every click event handler
		$(".filter a").click(function(e){
			$(".filter li").removeClass("current");	
			
			var $filterClass = $(this).parent().attr("class");
			if ( $filterClass == "all" ) {
				var $filteredPortfolio = $portfolioClone.find("li");
			} else {
				var $filteredPortfolio = $portfolioClone.find("li[data-type~=" + $filterClass + "]");
			}
		// Call quicksand
			$(".portfolio").quicksand( $filteredPortfolio, { 
				duration: 1000, 
				easing: 'easeInOutElastic',
				adjustHeight: 'dynamic'
			});
			$(this).parent().addClass("current");	
			e.preventDefault();
		});

	// HoverDir After quicksand
		$("ul.portfolio").hover(function(e){
			$('.portfolio > li').hoverdir();
		});
});