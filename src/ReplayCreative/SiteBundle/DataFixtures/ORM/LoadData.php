<?php

namespace ReplayCreative\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ReplayCreative\BlogBundle\Entity\Post;
use ReplayCreative\BlogBundle\Entity\Tag;
use ReplayCreative\UserBundle\Entity\User;
class LoadData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('Martin Nguyen');
        $userAdmin->setEmail('anthony@replaycreative.com');
        $userAdmin->setPassword('temp');
        $userAdmin->setBio('Martin Nguyen is the 24 year-old owner and lead developer at DragonIT. He made his first website at 12 years old. Since the age of 16, he has participated in and has founded several internet start-ups. He is an expert PHP, Symfony and frontend programmer.');
        $userAdmin->setImageUrl('bundles/replaycreativesite/images/anthony_100x100.png');

        $post = new Post();
        $post->setTitle('First post!');
        $post->setBody("Hello Everyone!

My name is Martin Nguyen and I'm the owner and lead developer at DragonIT. My intention for this blog is to record and share some of the challenges and solutions we come across at DragonIT throughout our daily work.

Some of the technologies that we love to use at DragonIT include: PHP, Ruby, Symfony 2 & 1, Ruby on Rails, HTML5, CSS3, Node.js, Backbone.js, BASH, Silex, and Git. These are just a select few, and we're adding new technologies to our arsenal frequently.

Check back often for more posts!");
        $post->setShortDescription("My name is Martin Nguyen and I'm the owner and lead developer at DragonIT. My intention for this blog is to record and share some of the challenges and solutions we come across at DragonIT throughout our daily work. ");
        $post->setSlug('first-post');
        $post->setIsActive(true);
        $post->setAuthor($userAdmin);

        $tags = explode(',', 'symfony,symfony2,PHP,Ruby,Ruby on Rails,HTML5,CSS3,Node.js,Backbone.js,BASH,Silex,Git');
        foreach ($tags as $tag) {
            $t = new Tag();
            $t->setName($tag);
            $manager->persist($t);
            $post->addTag($t);
        }
    
        $manager->persist($post);
        $manager->persist($userAdmin);
        $manager->flush();
    }
}