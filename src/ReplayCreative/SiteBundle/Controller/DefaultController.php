<?php

namespace ReplayCreative\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ReplayCreative\SiteBundle\Entity\Contact;
use ReplayCreative\SiteBundle\Form\ContactType;

class DefaultController extends Controller
{
    public function indexAction()
    {
//    	$posts = $this->getDoctrine()->getManager()->getRepository('ReplayCreativeBlogBundle:Post')->getLastActive(2);
        return $this->render('ReplayCreativeSiteBundle:Default:home.html.twig');
    }
    
    /**
     * Displays a form to create a new Contact entity.
     * Creates a new Contact entity.
     *
     */
    public function contactAction(Request $request)
    {
        if($request->get('form_type') !== 'bare') return $this->redirect('/');
 		   	$em = $this->getDoctrine()->getManager();
	    	$entity = new Contact();
	    	$contactForm   = $this->createForm(new ContactType(), $entity);
        $response = new Response();
        $template = ($request->get('form_type') == 'bare') ? 'ReplayCreativeSiteBundle:Default:contact_form.html.twig' : 'ReplayCreativeSiteBundle:Default:contact.html.twig';
  	
	    	if($request->isMethod('post'))
	    	{
		    		$contactForm->bind($request);
		    		if ($contactForm->isValid() && $this->checkHoneypot($contactForm, $request))
		    		{			    			
                
			    			$em->persist($entity);
			    			$em->flush();
			    			
			    			$this->sendEmail($entity);
			    			
			    			return $this->redirect($this->generateUrl('contact_success'));
		    		}
            else {
              $response->setStatusCode(412);
              $response->headers->set('Content-Type', 'text/html');
            }
	    	}
	    	
	    	return $this->render($template, array(
	    			'entity' => $entity,
            'form_type' => $request->get('form_type'),
	    			'contactForm'   => $contactForm->createView(),
	    	), $response);
    }
    private function checkHoneypot($form, $request) {
        $honeypot = $request->request->all();
        $honeypot = $honeypot[$form->getName()]['leave_blank'];
        if ($honeypot !== 'replaycreativeftw') {
          return false;
        } return true;
    } 
    private function sendEmail(Contact $contact)
    {
				$tpl = $this->renderView('ReplayCreativeSiteBundle:Default:contact_email_template.html.twig', array(
								'contact'	=>	$contact
						));    	
    	
	    	$message = \Swift_Message::newInstance()
		    	->setSubject('New Contact Message | DragonIT')
		    	->setFrom($contact->getEmail())
		    	->setTo($this->container->getParameter('admin_email'))
		    	->setBody(
		    			html_entity_decode($tpl),
		    			'text/html'
		    	);
	    	$this->container->get('mailer')->send($message);    	
    }
    
    public function contactSuccessAction(Request $request)
    {
    		return $this->render('ReplayCreativeSiteBundle:Default:contact_success.html.twig');
    }
}
