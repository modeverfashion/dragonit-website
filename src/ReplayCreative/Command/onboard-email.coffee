showHelp = () ->
  console.log """
    how to run: onboard-email.coffee name email redmineusername title password
    arg 1: name
    arg 2: email
    arg 3: redmine username
    arg 4: title
    arg 5: password
  """
  process.exit()


text = """
Hi [firstname],

Welcome to DragonIT. We’re really excited to have you join our team!

This email contains important information regarding:
 - Your DragonIT email address
 - Our project management system
 - Logging and tracking your hours
 - Required forms to fill out and return

We have a growing team of talented people working at DragonIT and we're thrilled that you are a part of that.

Best,
Martin Nguyen
Founder, DragonIT

---------------------------------------------

Email Login Instructions:

All of our contractors have a DragonIT email address to simplify communications, reinforce branding and allow for direct client communications if the need arises. Please see below for your login instructions. It's important to follow the instructions because we'll be using this email address to contact you about new jobs, tasks and opportunities. 

Go to: http://web-mail.replaycreative.com
username: [email]
password: [password]

Please configure your email client to receive and send messages as [email]

Additionally please add the following signature to all messages sent from [email]:

[name]
[title]
DragonIT
t. (888) 767-9186
w. http://replaycreative.com


--------------------------------------------------

Project Management Login Instructions:
Go to: https://projects.replaycreative.com
username: [redmine.username]
password: [password]

Time Tracking Instructions:

It’s very important to watch the video below so you get paid, on time and for all of the work you do!

https://www.youtube.com/watch?v=xdVjV9O1KOI

--------------------------------------------------

Legal Documents

**** All consultants must sign and return the following form and initial each page before commencing work: https://drive.google.com/file/d/0BxfUlziGQsvsLV9vY3AyUzd4aFk ****

If you not a United States citizen please sign and return the W-8BEN form found here: http://www.irs.gov/pub/irs-pdf/fw8ben.pdf

If you are a United States citizen please sign and return the W-9 form found here: http://www.irs.gov/pub/irs-pdf/fw9.pdf

You will not be able to get paid until you submit these forms.

---------------------------------------------------

"""


showHelp() unless process.argv[2]? and process.argv[3]? and process.argv[4]? and process.argv[5]? and process.argv[6]?

  
name = process.argv[2]
email = process.argv[3]
redmine = process.argv[4]
title = process.argv[5]
pass = process.argv[6]

text = text.replace /\[name\]/g, name
text = text.replace /\[firstname\]/g, name.split(' ')[0]
text = text.replace /\[email\]/g, email
text = text.replace /\[title\]/g, title
text = text.replace /\[password\]/g, pass
text = text.replace /\[redmine\.username\]/g, redmine

console.log text
