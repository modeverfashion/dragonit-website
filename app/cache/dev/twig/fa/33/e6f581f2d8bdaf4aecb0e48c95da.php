<?php

/* ReplayCreativeSiteBundle:Default:contact_form.html.twig */
class __TwigTemplate_fa33e6f581f2d8bdaf4aecb0e48c95da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t\t\t\t";
        // line 2
        echo "\t\t\t\t<form method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contact_page"), "html", null, true);
        if (((isset($context["form_type"]) ? $context["form_type"] : $this->getContext($context, "form_type")) == "bare")) {
            echo "?form_type=bare";
        }
        echo "\" id=\"contact-form\" novalidate=\"novalidate\">
\t\t\t\t\t
\t\t\t\t\t\t";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "_token"), 'row');
        echo " 

          <div class=\"form-row clearfix\">
              ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "name"), 'label');
        echo "
              ";
        // line 8
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "name"), "vars"), "errors")) < 1)) {
            // line 9
            echo "                ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "name"), 'widget', array("attr" => array("placeholder" => "Required")));
            echo "
                ";
        } else {
            // line 11
            echo "                ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "name"), 'widget', array("attr" => array("placeholder" => "Required", "class" => "false")));
            echo "
              ";
        }
        // line 13
        echo "          </div>
                            
          <div class=\"form-row clearfix\">
\t\t\t\t\t\t";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "email"), 'label');
        echo "
\t\t\t\t\t\t";
        // line 18
        echo "            ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "email"), "vars"), "errors")) < 1)) {
            // line 19
            echo "\t\t\t\t\t    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "email"), 'widget', array("attr" => array("placeholder" => "Required")));
            echo "
            ";
        } else {
            // line 21
            echo "\t\t\t\t\t    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "email"), 'widget', array("attr" => array("placeholder" => "Required", "class" => "false")));
            echo "
            ";
        }
        // line 23
        echo "          </div>
                            
          <div class=\"form-row clearfix textbox\">
\t\t\t\t\t\t";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "message"), 'label');
        echo "
            ";
        // line 27
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "message"), "vars"), "errors")) < 1)) {
            // line 28
            echo "\t\t\t\t\t    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "message"), 'widget', array("attr" => array("placeholder" => "Your Message", "rows" => 15, "cols" => 50)));
            echo "
              ";
        } else {
            // line 30
            echo "\t\t\t\t\t    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "message"), 'widget', array("attr" => array("placeholder" => "Your Message", "rows" => 15, "cols" => 50, "class" => "false")));
            echo "
            ";
        }
        // line 32
        echo "          </div>
\t\t\t\t\t    ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "leave_blank"), 'widget');
        echo "
            
          ";
        // line 35
        if ((!twig_test_empty($this->getAttribute((isset($context["contactForm"]) ? $context["contactForm"] : $this->getContext($context, "contactForm")), "get", array(0 => "errors"), "method")))) {
            echo " 
            <div class=\"alert alert-error\">
              <h6><strong>Error</strong>: Please check your entries!</h6>
            </div>
          ";
        }
        // line 40
        echo "                            
          <div class=\"form-row form-submit\">
              <input type=\"submit\" name=\"submit_form\" class=\"submit\" value=\"Send\" />
          </div>
    
        
        </form> 
        <script type=\"text/javascript\">
          \$('#contact-form').submit(function(e) {
            e.preventDefault();
            \$('#replaycreative_sitebundle_contacttype_leave_blank').val('replaycreativeftw');
            \$.post(\$(this).attr('action'), \$(this).serialize())
              .always(function() {
                \$('#form-container input').each(function() {
                  \$(this).removeClass('false');
                  \$('input[name=submit_form]').attr('disabled', 'disabled');
                });
              })
              .done(function() {
                \$('#form-container').html(\"<h2>Thanks for contacting us! We'll be in touch within a day.</h2>\");
              })
              .fail(function(data) {
                \$(\"#form-container\").html(data.responseText);
              })
          });
        </script>
          

";
    }

    public function getTemplateName()
    {
        return "ReplayCreativeSiteBundle:Default:contact_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 40,  109 => 35,  104 => 33,  101 => 32,  95 => 30,  89 => 28,  87 => 27,  83 => 26,  78 => 23,  66 => 19,  63 => 18,  59 => 16,  54 => 13,  48 => 11,  42 => 9,  40 => 8,  36 => 7,  30 => 4,  85 => 62,  72 => 21,  70 => 50,  810 => 786,  781 => 760,  21 => 2,  19 => 1,);
    }
}
