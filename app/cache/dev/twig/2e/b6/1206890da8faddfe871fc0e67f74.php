<?php

/* ReplayCreativeSiteBundle:Default:footer.html.twig */
class __TwigTemplate_2eb61206890da8faddfe871fc0e67f74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "  <!-- FOOTER -->  
    <footer>
      <div class=\"footerinner wrapper align-center text-light\">
        <a id=\"backtotop\" href=\"#\" class=\"sr-button sr-buttonicon small-iconbutton\"><i class=\"fa fa-angle-up\"></i></a>
              ";
        // line 15
        echo "                <p class=\"copyright\">Copyright &copy; 2014 - DragonIT</p>
            </div>
      </footer>
        <!-- FOOTER -->         
        
  </div> <!-- END .page-body -->
  <!-- PAGEBODY -->
    
</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->


<!-- SCRIPTS -->
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/retina.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.easing.compatibility.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.visible.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.easy-opener.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.flexslider.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.isotope.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.bgvideo.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.fitvids.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/jplayer/jquery.jplayer.min.js'></script>
<script type=\"text/javascript\" src=\"/bundles/replaycreativesite/theme/files/rs-plugin/js/jquery.themepunch.plugins.min.js\"></script>
<script type=\"text/javascript\" src=\"/bundles/replaycreativesite/theme/files/rs-plugin/js/jquery.themepunch.revolution.min.js\"></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.parallax.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.counter.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/jquery.scroll.min.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/xone-header.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/xone-loader.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/xone-form.js'></script>
<script type='text/javascript' src='/bundles/replaycreativesite/theme/files/js/script.js'></script>
<!-- SCRIPTS -->
<style>
div.more {
  display: inline-block;
  display: none;
}
</style>
<script type=\"text/javascript\">
\$('a.seemore').click(function() {
  \$(this).hide();
  \$(this).parent().parent().find('.more').fadeIn();
});
\$('a.seeless').click(function() {
  \$(this).parent().hide();
  \$(this).parent().parent().find('.seemore').show();
});
</script>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "ReplayCreativeSiteBundle:Default:footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 15,  1191 => 330,  1185 => 329,  1179 => 328,  1173 => 327,  1167 => 326,  1161 => 325,  1155 => 324,  1149 => 323,  1143 => 322,  1127 => 316,  1120 => 315,  1118 => 314,  1115 => 313,  1092 => 309,  1067 => 308,  1065 => 307,  1062 => 306,  1050 => 301,  1045 => 300,  1043 => 299,  1040 => 298,  1031 => 292,  1025 => 290,  1022 => 289,  1017 => 288,  1015 => 287,  1012 => 286,  1005 => 281,  996 => 279,  992 => 278,  989 => 277,  986 => 276,  984 => 275,  981 => 274,  973 => 270,  971 => 269,  968 => 268,  961 => 263,  958 => 262,  950 => 257,  946 => 256,  942 => 255,  939 => 254,  937 => 253,  934 => 252,  926 => 248,  924 => 244,  922 => 243,  919 => 242,  897 => 235,  894 => 234,  891 => 233,  888 => 232,  885 => 231,  882 => 230,  879 => 229,  876 => 228,  873 => 227,  870 => 226,  867 => 225,  865 => 224,  862 => 223,  854 => 217,  851 => 216,  849 => 215,  846 => 214,  838 => 210,  835 => 209,  833 => 208,  830 => 207,  822 => 203,  819 => 202,  817 => 201,  814 => 200,  806 => 196,  803 => 195,  801 => 194,  798 => 193,  790 => 189,  787 => 188,  785 => 187,  782 => 186,  774 => 182,  771 => 181,  769 => 180,  766 => 179,  758 => 175,  756 => 174,  753 => 173,  745 => 169,  742 => 168,  740 => 167,  737 => 166,  729 => 162,  726 => 161,  724 => 160,  722 => 159,  719 => 158,  712 => 153,  702 => 152,  697 => 151,  694 => 150,  688 => 148,  685 => 147,  683 => 146,  680 => 145,  672 => 139,  670 => 138,  669 => 137,  668 => 136,  667 => 135,  662 => 134,  656 => 132,  653 => 131,  651 => 130,  648 => 129,  639 => 123,  635 => 122,  631 => 121,  627 => 120,  622 => 119,  616 => 117,  613 => 116,  611 => 115,  608 => 114,  592 => 110,  590 => 109,  587 => 108,  571 => 104,  569 => 103,  566 => 102,  549 => 98,  537 => 96,  530 => 93,  528 => 92,  523 => 91,  520 => 90,  502 => 89,  500 => 88,  497 => 87,  488 => 82,  485 => 81,  482 => 80,  476 => 78,  474 => 77,  469 => 76,  466 => 75,  463 => 74,  450 => 72,  448 => 71,  440 => 70,  438 => 69,  435 => 68,  429 => 64,  421 => 62,  416 => 61,  412 => 60,  407 => 59,  405 => 58,  402 => 57,  393 => 52,  387 => 50,  384 => 49,  382 => 48,  379 => 47,  369 => 43,  367 => 42,  364 => 41,  356 => 37,  353 => 36,  350 => 35,  347 => 34,  345 => 33,  342 => 32,  334 => 27,  329 => 26,  323 => 24,  321 => 23,  316 => 22,  314 => 21,  311 => 20,  295 => 16,  292 => 15,  290 => 14,  287 => 13,  278 => 8,  272 => 6,  269 => 5,  267 => 4,  264 => 3,  260 => 330,  258 => 329,  256 => 328,  254 => 327,  252 => 326,  250 => 325,  248 => 324,  246 => 323,  244 => 322,  241 => 321,  238 => 319,  236 => 313,  233 => 312,  231 => 306,  228 => 305,  226 => 298,  223 => 297,  220 => 295,  218 => 286,  215 => 285,  213 => 274,  210 => 273,  208 => 268,  205 => 267,  202 => 265,  200 => 262,  197 => 261,  195 => 252,  192 => 251,  190 => 242,  187 => 241,  184 => 239,  182 => 223,  179 => 222,  176 => 220,  174 => 214,  171 => 213,  169 => 207,  166 => 206,  164 => 200,  161 => 199,  159 => 193,  156 => 192,  154 => 186,  151 => 185,  149 => 179,  146 => 178,  144 => 173,  141 => 172,  139 => 166,  136 => 165,  134 => 158,  131 => 157,  129 => 145,  126 => 144,  124 => 129,  121 => 128,  119 => 114,  116 => 113,  114 => 108,  111 => 107,  106 => 101,  99 => 68,  96 => 67,  94 => 57,  91 => 56,  86 => 46,  84 => 41,  81 => 40,  79 => 32,  76 => 31,  74 => 20,  71 => 19,  69 => 13,  64 => 3,  61 => 2,  117 => 40,  109 => 102,  104 => 87,  101 => 86,  95 => 30,  89 => 47,  87 => 27,  83 => 26,  78 => 23,  66 => 12,  63 => 18,  59 => 16,  54 => 13,  48 => 11,  42 => 9,  40 => 8,  36 => 7,  30 => 4,  85 => 62,  72 => 21,  70 => 50,  810 => 786,  781 => 760,  21 => 2,  19 => 1,);
    }
}
